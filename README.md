# The Linux Programming Interface

Code examples and exercises from [The Linux Programming Interface](http://man7.org/tlpi/) by Michael Kerrisk.
