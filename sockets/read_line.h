ssize_t readLine(int fd, void *buffer, size_t n);
        /* Returns number of bytes copied into buffer (excluding
         * terminating null byte), or 0 on end-of-file, or
         * -1 on error */
