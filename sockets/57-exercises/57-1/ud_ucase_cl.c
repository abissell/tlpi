#include "ud_ucase.h"

int
main(int argc, char *argv[])
{
    struct sockaddr_un svaddr, claddr;
    int sfd, j;
    size_t msgLen;
    ssize_t numBytes;
    char resp[BUF_SIZE];

    if (argc < 2 || strcmp(argv[1], "--help") == 0)
        usageErr("%s msg...\n", argv[0]);

    /* Create client socket; bind to unique pathname (based on PID) */

    sfd = socket(AF_UNIX, SOCK_DGRAM, 0);
    if (sfd == -1)
        errExit("socket");

    memset(&claddr, 0, sizeof(struct sockaddr_un));
    claddr.sun_family = AF_UNIX;
    snprintf(claddr.sun_path, sizeof(claddr.sun_path),
            "/tmp/ud_ucase_cl.%ld", (long) getpid());

    if (bind(sfd, (struct sockaddr *) &claddr, sizeof(struct sockaddr_un)) == -1)
        errExit("bind");

    /* Construct address of server */

    memset(&svaddr, 0, sizeof(struct sockaddr_un));
    svaddr.sun_family = AF_UNIX;
    strncpy(svaddr.sun_path, SV_SOCK_PATH, sizeof(svaddr.sun_path) - 1);

    /* Rapidly send messages to server */

    for (j = 0; j < 1000; j++) {
        msgLen = strlen(argv[0]); /* May be longer than BUF_SIZE */
        printf("Client trying to send message to server ...\n");
        if (sendto(sfd, argv[0], msgLen, 0, (struct sockaddr *) &svaddr,
                    sizeof(struct sockaddr_un)) != msgLen)
            fatal("sendto");
        printf("Client successfully sent %d messages to server.\n", (j+1));
    }

    printf("Client finished sending all messages\n");

    remove(claddr.sun_path); /* Remove client socket pathname */
    exit(EXIT_SUCCESS);
}
