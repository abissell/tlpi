#include "ud_nonpeer_send.h"

int
main(int argc, char *argv[])
{
    struct sockaddr_un addra, addrb;
    int afd, bfd, sendfd;
    size_t msgLen;
    ssize_t numBytes;

    /* Create first two sockets and bind to /tmp/somepath_ */

    afd = socket(AF_UNIX, SOCK_DGRAM, 0);
    if (afd == -1)
        errExit("socket a");

    memset(&addra, 0, sizeof(struct sockaddr_un));
    addra.sun_family = AF_UNIX;
    strncpy(addra.sun_path, "/tmp/somepath_a", sizeof(addra.sun_path) - 1);
    if (bind(afd, (struct sockaddr *) &addra,
                sizeof(struct sockaddr_un)) == -1)
        errExit("bind a");

    bfd = socket(AF_UNIX, SOCK_DGRAM, 0);
    if (bfd == -1)
        errExit("socket b");

    memset(&addrb, 0, sizeof(struct sockaddr_un));
    addrb.sun_family = AF_UNIX;
    strncpy(addrb.sun_path, "/tmp/somepath_b", sizeof(addrb.sun_path) - 1);
    if (bind(bfd, (struct sockaddr *) &addrb,
                sizeof(struct sockaddr_un)) == -1)
        errExit("bind b");

    /* Connect the a socket to the b socket */

    if (connect(afd, (struct sockaddr *) &addrb,
                sizeof(struct sockaddr_un)) == -1)
        errExit("connect a to b");

    /* Create the third socket and try to sendto() /tmp/somepath_a */

    sendfd = socket(AF_UNIX, SOCK_DGRAM, 0);
    if (sendfd == -1)
        errExit("socket send");

    char *msg = "hello";
    numBytes = 6;
    numBytes = sendto(sendfd, msg, numBytes, 0, (struct sockaddr *) &addra,
            sizeof(struct sockaddr_un));
    if (numBytes != 6) {
        printf("send failed, had numBytes %d", (int) numBytes);
        errExit("sendto");
    }

    /* Remove socket pathnames */

    remove(addra.sun_path);
    remove(addrb.sun_path);
    exit(EXIT_SUCCESS);
}
