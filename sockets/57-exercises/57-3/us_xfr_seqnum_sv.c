#include "us_xfr_seqnum.h"
#define BACKLOG 20

int
main(int argc, char *argv[]) {
    struct sockaddr_un addr;
    int sfd, cfd;
    int seqNumReq;

    sfd = socket(AF_UNIX, SOCK_STREAM, 0);
    if (sfd == -1)
        errExit("socket");

    /* Construct server socket address, bind socket to it,
       and make this a listening socket */

    if (strlen(SV_SOCK_PATH) > sizeof(addr.sun_path) - 1)
        fatal("Server socket path too long: %s", SV_SOCK_PATH);

    if (remove(SV_SOCK_PATH) == -1 && errno != ENOENT)
        errExit("remove-%s", SV_SOCK_PATH);

    memset(&addr, 0, sizeof(struct sockaddr_un));
    addr.sun_family = AF_UNIX;
    strncpy(addr.sun_path, SV_SOCK_PATH, sizeof(addr.sun_path) - 1);

    if (bind(sfd, (struct sockaddr *) &addr, sizeof(struct sockaddr_un)) == -1)
        errExit("bind");

    if (listen(sfd, BACKLOG) == -1)
        errExit("listen");

    int seqNum = 0; /* This is our "service" */
    for (;;) { /* Handle client connections iteratively */
        cfd = accept(sfd, NULL, NULL);
        if (cfd == -1)
            errExit("accept");

        /* Should receive exactly sizeof(int) bytes from client
           for seqNum range request */
        if (read(cfd, &seqNumReq, sizeof(int)) != sizeof(int))
            fatal("partial/failed read");

        if (write(cfd, &seqNum, sizeof(int)) != sizeof(int))
            fatal("partial/failed write");

        if (close(cfd) == -1)
            errMsg("close");

        seqNum += seqNumReq;
        printf("SERVER: Granted requested seqNum range of %d\n", seqNumReq);
        printf("SERVER: seqNum is now %d\n", seqNum);
    }
}
