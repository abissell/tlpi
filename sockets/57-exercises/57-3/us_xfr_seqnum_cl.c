#include "us_xfr_seqnum.h"

int
main(int argc, char *argv[])
{
    struct sockaddr_un addr;
    int sfd;
    int seqNum;

    memset(&addr, 0, sizeof(struct sockaddr_un));
    addr.sun_family = AF_UNIX;
    strncpy(addr.sun_path, SV_SOCK_PATH, sizeof(addr.sun_path) - 1);

    int reqRange = getInt(argv[1], 0, "seqNumRange");
    printf("CLIENT %d: requesting seqNum range of %d every 5 seconds\n", getpid(), reqRange);

    for (;;) {
        sfd = socket(AF_UNIX, SOCK_STREAM, 0);
        if (sfd == -1)
            errExit("socket");

        if (connect(sfd, (struct sockaddr *) &addr,
                    sizeof(struct sockaddr_un)) == -1)
            errExit("connect");

        printf("CLIENT %d: requesting seqNum range of %d\n", getpid(), reqRange);
        if (write(sfd, &reqRange, sizeof(int)) != sizeof(int))
            fatal("partial/failed write");

        if (read(sfd, &seqNum, sizeof(int)) != sizeof(int))
            fatal("partial/failed read");

        printf("CLIENT %d: received range, seqNum is now %d\n", getpid(), seqNum);

        sleep(5);
    }
}
